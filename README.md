# 简介

这是个人兴趣发起的一些关于明日方舟的一些数值计算、统计、回归分析和实用小工具，不定期更新，可能会一直鸽下去，一切取决于我的心情和工作是否忙。如有兴趣可以一起制作或根据此项目发起一个分支等方式，都是可以接受的。

This is some numerical calculations, statistics, regression analysis and practical gadgets about Tomorrow’s Ark initiated by personal interest. It is updated from time to time and may be shelved forever, it all depends on my mood and whether my work is busy. If you are interested, you can make it together or initiate a branch based on this project etc., all acceptable.

目前有以下子项目在开发计划中:

1. 自动辅助制表(Automatic Auxiliary Tabulation)
2. 爬取干员信息(Operator Information Database)
3. 对应理论(Correspondence Theory)
4. 地图难度计算(Map Difficulty Calculation)

# 1.自动辅助制表

1. [X] av,BV和视频链接(https地址)之间的转换
2. [X] 根据BV或链接获取视频发布时间
3. [ ] 自动填写腾讯在线文档(difficult)
4. [ ] 在arkrec.com提交信息(difficult)

# 2.干员信息库

1. [X] 建立数据库
2. [ ] 从wiki爬取干员信息
3. [ ] 从数据库导出信息到Excel表格
4. [ ] DPS计算(difficult)

# 3.对应理论

* [X] 收集不同流派的极限人数信息
* [X] 两两组合线性回归
* [ ] 假定新的对应关系(difficult)
* [ ] 根据对应关系进行回归计算
* [ ] 评估准确性与合理性,找到最优方案(difficult)

# 4.地图难度计算

* [ ] 导入地图信息(difficult)
* [ ] 随机生成地图(difficult)
* [ ] 分析，量化影响因素(difficult)
* [ ] 根据所有影响因素得出总难度
* [ ] 评估准确性与合理性,找到最优方案(difficult)
